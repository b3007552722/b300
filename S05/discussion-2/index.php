<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05 Discussion: Client Server Communication (Basic Task App/To-Do List)</title>
</head>
<body>

    <?php session_start(); ?>
    <!-- The session variable are stored on the server, and they are not directly viewble in the browser. -->
    <!-- This is typically placed at the beginning of the php script -->
    <!-- Session data is stored on the server and also allows the server to identify the user's session on multiple requests. -->

    <h1>S05 Discussion: Client Server Communication (Basic Task App/To-Do List)</h1>
    <hr>

    <h3>Add Task</h3>

	<!-- action let us go to a certain page or file -->
	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="add"/>
		Description: <input type="text" name="description" required/>
		<button type="submit">Add</button>
	</form>


	<pre><?php var_dump($_SESSION['tasks']) ?></pre>



	<h3>Task List</h3>
    <!-- We want to view our tasklist -->

    <?php if (isset($_SESSION['tasks'])): ?>

        <?php foreach ($_SESSION['tasks'] as $index => $task): ?>

        <div>
            <form method="POST" action="./server.php" style="display:inline-block;">
                <input type="hidden" name="action" value="update"/>

                <p type="hidden" name="id" value="<?php echo $index; ?>"></p>
                <!-- This is the value of the index (serve as the id of the task) of the element of the array (SESSION) -->
                <input type="checkbox" name="isFinished" <?php echo ($task->isFinished) ? 'checked' :  null; ?>/>
                <!-- We may know that a task is already finished, when there is a check -->

                <input type="text" name="description" value="<?php echo $task->description; ?>"/>


                <input type="submit" value="Update"/>
            </form>
            
            <form method="POST" action="./server.php" style="display:inline-block;">
                <input type="hidden" name="action" value="remove"/>
                <input type="hidden" name="id" value="<?php echo $index; ?>"/>
                <input type="submit" value="Delete"/>
            </form>

        </div>


        <?php endforeach; ?>

    <?php  endif; ?>



	<br/><br/>

	<h3>Delete All Tasks</h3>

	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="clear">
		<button type="submit">Clear all tasks</button>
	</form>

</body>
</html>