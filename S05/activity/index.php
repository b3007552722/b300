<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S05 Activity: Client Server Communication (Basic Task login/logout)</title>
</head>
<body>
    <?php session_start(); ?>

    <h1>S05 Activity: Client Server Communication (Basic Task login/logout)</h1>
    <hr>

    <?php if(!isset($_SESSION['users'])): ?>

    <h2>Login Here!</h2>
    <form method="POST" action="./server.php">
		<input type="hidden" name="action" value="login"/>
		User Email: <input type="email" name="email" required/>
        User Password: <input type="password" name="password" required/>
        <br><br>
		<button type="submit">LOGIN USER</button>
	</form>
    
    <?php else: ?>
    <h2>Welcome to Dashboard!</h2>
    <h3>Hello, <?php echo $_SESSION['users'][0]->email ?></h3>

    <form method="POST" action="./server.php">
		<input type="hidden" name="action" value="logout">
		<button type="submit">LOGOUT USER</button>
	</form>

    <?php endif; ?>
</body>
</html>