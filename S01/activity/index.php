<!-- Connecting the code.php here in index -->
<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Short Course S1 (Activity)</title>
</head>
<body>

    <h1>Full Address</h1>
    <p><?= getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F Caswynn Bldg., Timog Avenue'); ?></p>

    <p><?= getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F Enzo Bldg., Buendia Avenue'); ?></p>

    <h1>Letter-Based Grading</h1>
    <p>87 is equivalent to <?= getLetterGrade(87); ?></p>
    <p>94 is equivalent to <?= getLetterGrade(94); ?></p>
    <p>74 is equivalent to <?= getLetterGrade(74); ?></p>



</body>
</html>