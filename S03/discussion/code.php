<?php

/* 
    - [1] Objects as Variable -
*/

$buildingObj = (object)[
    'name' => 'Caswynn Building',
    'floors' => 8,
    'address' => (object)[
        'barangay' => 'Sacred Heart',
        'city' => 'Quezon City',
        'country' => 'Philippines'
    ]
];

$buildingObj2 = (object)[
    "name" => "GMA Network",
    "floors" => 20,
    "address" => "EDSA corner Timog Avenue, Diliman, Quezon City"
];

/* 
    - [2] Objects from Classes -
*/

class Building {

    // This represents a class named Building
    /*
        1. Properties - Characteristics of an object
    */
    public $name;
    public $floors;
    public $address;

    /* 
        2. Constructor Function - The class also has a constructor method __construct() that accepts parameters to initialize the properties of the object.
    */
    public function __construct($name, $floors, $address){

        /* 
            * "$this" keyword refers to the properties and methods within the class.
            * "$this-name" is accesssing the "name" property of the current class (Building) and assigning the value of the $name upon instantiation of an object.
        */
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    /* 
        3. Method - These are functions inside of an object that can perform a specific action.
    */
    public function printName(){
        return "The name of this building is $this->name";
    }

};

/* 
    Instantiating Building Class to create a new building object.
*/

$building = new Building(
    "Caswynn Building", 
    8, 
    "Timog Avenue, Quezon City, Philippines"
);

/* 
    - [3] Inheritance and Polymorphism
        Inheritance - The derived classes are allowed to inherit properties and methods from a specified base class.
        The "extends" keyword is used to inherit the properties and methods of a base or parent class.
    -
*/

// Parent class => Building
// Child class => Condominum

Class Condominium extends Building{

    //the building properties and methods are inherited in this class

    //Polymorphism - method inherited by the derived class can be overidden to have a behavior different from the method of the base class

    public function printName(){
        return "The name of this condominium is $this->name";
    }

}

$condominium = new Condominium (
    "Enzo Condo", 
    5, 
    "Buendia Avenue, Makati City, Philippines"
);


/* 
    - Mini Activity
        Create a pokemon constructor (5mins.)
            - properties:
                type
                level
                trainer
            -
            - methods:
                attack
                faint
            -
        Instantiate 3 pokemon objects/Instances
        Display the 3 objects in the webpage using var_dump
    -
*/

class Pokemon {

    public $name;
    public $type;
    public $level;
    public $trainer;

    public function __construct($name, $type, $level, $trainer){

        $this->name = $name;
        $this->type = $type;
        $this->level = $level;
        $this->trainer = $trainer;
    }

    public function printName(){
        return "";
    }
};

$pokemon = new Pokemon(

    "Pikachu",
    "Thunder", 
    15, 
    "Ash"
);

$pokemon2 = new Pokemon(
    
    "Bulbasaur",
    "Grass", 
    8, 
    "Ash"
);

$pokemon3 = new Pokemon(
    
    "charmander",
    "fire", 
    6, 
    "Ash"
);


class Animal {

    public $name;

    public function __construct($name){
        $this->name = $name;
    }
}

class Dog extends Animal {
    public $breed;

    public function __construct($name, $breed){
        parent::__construct($name);
        $this->breed = $breed;
    }
}

$dog1 = new Dog("whitey", "Chihuahua");