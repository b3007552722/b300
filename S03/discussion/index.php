
<?php require_once "./code.php" ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03 Discussion: Classes and Objects</title>
</head>
<body>
    <h1>S03 Discussion: Classes and Objects</h1>
    <hr>
    <h2>Objects from Variable</h2>
    <p><?php echo $buildingObj->name; ?></p>
    <p><?php echo $buildingObj->address->city; ?></p>
    <p><?= print_r($buildingObj) ?></p>
    <p><?php echo $buildingObj2->address; ?></p>
    <hr>

    <h2>Object from Classes</h2>
    <p><?php var_dump($building); ?></p>

    <h3>Modify the Instantiated Object</h3>
    <?php $building->name = "GMA Network"; ?>
    <?php $building->floors = "Twenty-one"; ?>
    <p><?php var_dump($building) ?></p>
    <p><?= $building->printName(); ?></p>
    <hr>

    <h2>Inheritance (Condominium Object)</h2>
    <p><?php var_dump($condominium) ?></p>
    <p><?= $condominium->name; ?></p>
    <p><?= $condominium->floors; ?></p>
    <p><?= $condominium->address; ?></p>
    <hr>

    <h2>Polymorphism (Overriding the behavior of the printName() method)</h2>
    <p><?= $condominium->printName(); ?></p>

    <h2>Object from Classes (Mini Activity - Pokemon)</h2>
    <p><?php var_dump($pokemon); ?></p>
    <p><?php var_dump($pokemon2); ?></p>
    <p><?php var_dump($pokemon3); ?></p>

    <p><?php var_dump($dog1) ?></p>

</body>
</html>