<?php

/* 
    - [Activity Instruction]

        -
        1. Create a Person class with three properties:
            firstName
            middleName
            lastName
        2. Create two child classes for Person:
            Developer
            Engineer
        3. Create a printName() method in each of the classes and must have the following output:
            Person: "Your full name is Senku Ishigami"
            Developer: "Your name is John Finch Smith and you are a  developer."
            Engineer: "You are an engineer named Harold Myers Reese."
        -
    -        
*/



/* Start Person */

class Person {

    public $name;
    public $floors;
    public $address;

    public function __construct($firstName, $middleName, $lastName){

        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){

        return "Your full name is $this->firstName $this->lastName.";
    }
};

$person = new Person(

    // firstName, middleName, lastName
    "Senku", "", "Ishigami"
);

/* End Person */





/* Start Developer */

Class Developer extends Person{

    public function printName(){
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a  developer.";
    }

}

$developer = new Developer(

    // firstName, middleName, lastName
    "John", "Finch", "Smith"
);

/* End Developer */





/* Start Enginer */

Class Engineer extends Person{

    public function printName(){
        return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
}

$engineer = new Engineer(

    // firstName, middleName, lastName
    "Harold", "Myers", "Reese"
);

/* End Enginer */