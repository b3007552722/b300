<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03 Supplementary Activity: Classes and Objects</title>
</head>
<body>
    <h1>S03 Supplementary Activity: Classes and Objects</h1>
    <hr>

    <h2>Voltes V </h2>
    <p><?php var_dump($steve); ?></p>
    <p><?php var_dump($bigBert); ?></p>
    <p><?php var_dump($littleJohn); ?></p>
    <p><?php var_dump($jamie); ?></p>
    <p><?php var_dump($mark); ?></p>

    <hr>
    <h2>Voltes Member</h2>
    <p><?= $steve->printName(); ?></p>
    <p><?= $bigBert->printName(); ?></p>
    <p><?= $littleJohn->printName(); ?></p>
    <p><?= $jamie->printName(); ?></p>
    <p><?= $mark->printName(); ?></p>
    
</body>
</html>