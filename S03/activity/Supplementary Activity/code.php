<?php

/* 
    - [Supplementary Activity Instruction]

        1. Create a Character class with a name property and printName() method.
        2. Create a derived Class named VoltesMember from the chracter class.
            - Add a vechile property in the VoltesMember character class.
            - Override the printName() class to include the VoltesMeber's vehicle.
        3. Instantiate the 5 VoltesMember objects/Instances.
        4. Display the 5 objects in the webpage using var_dump.

    -
*/

class VoltesMember {

    public $name;
    public $vehicle;

    public function __construct($name, $vehicle){

        $this->name = $name;
        $this->vehicle = $vehicle;
    }

    public function printName(){
        return "Hi, I am $this->name! I pilot the $this->vehicle.";
    }
};

// object variable = new (old variable) ("name", "vehicle")
$steve = new VoltesMember("Steve", "Volt Cruiser");
$bigBert = new VoltesMember("Big Bert", "Volt Panzer");
$littleJohn = new VoltesMember("Little John", "Volt Frigate");
$jamie = new VoltesMember("Jamie", "Volt Lander");
$mark = new VoltesMember("Mark", "Volt Bomber");
