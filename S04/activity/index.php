<?php require_once "./code.php"; ?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04 Activity: Access Modifier and Inheritance</title>
</head>
<body>
    <h1>S04 Activity: Access Modifier and Inheritance</h1>
    <hr>
    <h2>Building</h2>
    <p>The name of the building <?php echo $building->getName();?>.</p>
    <p>The <?php echo $building->getName();?> has <?php echo $building->getFloors();?> floors.</p>
    <p>The <?php echo $building->getName();?> is located at <?php echo $building->getLocation();?>. </p>
    <p>The name of the building has been changed to <?php echo $building->setName("Caswynn Tower");?>.</p>

    <hr>
    <h2>Condominium</h2>

    <p>The name of the condomonium <?php echo $condominium->getName();?>.</p>
    <p>The <?php echo $condominium->getName();?> has <?php echo $condominium->getFloors();?> floors.</p>
    <p>The <?php echo $condominium->getName();?> is located at <?php echo $condominium->getLocation();?>. </p>
    <p>The name of the condominium has been changed to <?php echo $condominium->setName("Enzo Tower");?>.</p>


<!DOCTYPE html>
<html>
</body>
</html>






