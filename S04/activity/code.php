<?php

class Building {


    //Properties
    protected $name;
    protected $floors;
    protected $address;

    // Constructor
    public function __construct($name, $floors, $address){

        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName(){
        return $this->name;
    }

    public function getFloors(){
        return $this->floors;
    }

    public function getLocation(){
        return $this->address;
    }

    public function setName($name) {
        
        
        return $this->name = $name;
    }

    private function setFloors($floors){
		if(gettype($floors) === "integer"){
			 return $this->floors = $floors;
		}
	}
	private function setLocation($address){
		if(gettype($address) === "string"){
			 $this->address = $address;
		}
	}

};

$building = new Building('Caswynn Building', 8, 'Timog Avenue,Quenzon City, Philippines');

class Condominium extends Building {
   
};

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

