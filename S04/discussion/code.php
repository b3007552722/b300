<?php

class Building {

    /* 
        - [Access Modifiers]
            These are keywords that can be used to control the "visibility" of properties and methods in a class
                1. public: fully open, properties or methods can be accessed from every.
                2. private: properties or methods can only be accessed within the class and disables INHERITANCE.
                3. protected: properties and methods is only accessible within the class and on its child class.
        -
    */

    //Properties
    protected $name;
    protected $floors;
    protected $address;

    // Constructor
    public function __construct($name, $floors, $address){

        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }
};

class Condominium extends Building {

    // These function serves as the intermediary in accessing the object's property
    // These function therefore defines whether an object's property can be accessed or change
    //Getters and Setters
        // These are used to implement the encapsulation of an object's data
        // These are used to retrieve and modify value of each property of the object
        //Each property of an object should have a set of getters and setters 

    // Ecapsulation - indicates that data must not be directly accessible to uses, but can be accessed only through public function (setter and getter function)

    //Getters and Setters

    //Getter - accessors 
        //this is used to retrieve/access the value of an instantiated object
    public function getName(){
        return $this->name;
    }

    //Setter - mutators
        //this is used to change the default value of a property of an instantiated object
        //Setter function can also be modified to add data validations
    public function setName($name){
        //$this->$name;

        if(gettype($name) === "string"){
            $this->name = $name;
        }
    }
};

$building = new Building('Caswynn Building', 8, 'Timog Avenue,Quenzon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');

// Mini-Acitivity
//Create a player class with these properties and methods
// Properties
    // protected - access modifiers
    // username
    // email
    // password
    // level
    // guild
// Methods
    // attack
    // defend

// Cretate a Mage Class that is derived from the player class
// Create a getter and setter to change and retrieve the usename of guild of the mage

// Instantiate three Mage objects/instantaces
//Use echo and the getter and setter functions

class Player {

	protected $username;
	protected $email;
	protected $password;
	protected $level;
	protected $guild;

	public function __construct($username, $email, $password,$level,$guild) {
	    $this->username = $username;
	    $this->email = $email;
	    $this->password = $password;
	    $this->level = $level;
	    $this->guild = $guild;
	}

    //method for attack:
	public function attack($enemy) {
		return "$this->username attacked $enemy";
	}
    //method for defense:
	public function defend(){
		return "$this->username defended itself!";
	}

}

class Mage extends Player{

    // getting the username to constructor of Player:
	public function getUsername() {
        return $this->username;
    }

    // setting (change) the username
    public function setUsername($username) {
        $this->username = $username;
    }

    // getting the guild to constructor of Player:
	public function getGuild() {
        return $this->guild;
    }

    // setting (change) the guild:
    public function setGuild($guild) {
        $this->guild = $guild;
    }

}


/* 
    [Setting a child class]
        $mage1 - Varaible Object
        Mage - inheritance
        ('') - ('username', '$email', '$password',' $level', '$guild')
        ; - limitter

*/
$mage1 = new Mage('Ran','Ran@mail.com','ran123',55,'Sleeping Knights');
$mage2 = new Mage('Yuuki','yuuki@mail.com','yukki45',70,'Sleeping Knights');
