
<?php require_once "./code.php"; ?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S04 Discussion: Access Modifier and Inheritance</title>
</head>
<body>

    <!-- For Copy purpose -->
    <?php ?>
    
    <h1>S04 Discussion: Access Modifier and Inheritance</h1>

    <hr>
    <h2>Access Modifier</h2>
    <h3>Building Variables</h3>
    <!-- <p><?php //echo $building->name ?></p> -->
    <h3>Condominium Variables</h3>
    <!-- <p><?php //echo $condominium->name ?></p> -->

    <hr>
    <h2>Encapsulation</h2>
    <p>The name of the condominium is 
        <?php echo $condominium->getName(); ?>.
    </p>
    
    <?php $condominium->setName(300)?>

    <p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>

    <hr>
    <h2>Mini Activity:</h2>

    <!-- 
        $mage1-> getUsername() 
            - function of public function getUsername() to get the stored $username objects in child class.
        $mage1-> getGuild()
            - function of public function getUsername() to get the stored $guild objects in child class.
    -->
    <p>Player: <?php echo $mage1->getUsername();?> of <?php echo $mage1->getGuild();?>.</p>

    <!-- 
        $mage1->attack($mage2->getUsername())
            - function of public function attack($enemy)to return a method.
        $mage2->defend()
            - function of public function defend() to return a method.
     -->
	<p><?php echo $mage1->attack($mage2->getUsername()); ?>!</p>
    <p><?php echo $mage2->defend(); ?>!</p>

    <!-- 
        $mage1->setGuild("Knights of the Blood")
            - function of public function setGuild($guild) to change the stored $guild object in the new object.
        $mage1-> getGuild()
            - function of public function getUsername() to get the stored $guild objects in child class.
     -->
	<p>She changed her guild to: <?php echo $mage1->setGuild("Knights of the Blood");?><?php echo $mage1->getGuild();?>!</p>

</body>
</html>