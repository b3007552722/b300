<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02 - Selection Control Structures and Array Manipulation</title>
</head>
<body>
    <h1>Divisible of Five</h1>

    <?php whileLoop(); ?>

    <h1>Array Manipulation</h1>

    <!-- 2. Accept a name of the student and add it to the student array. -->
    <?php array_push($students, 'John Smith'); ?>

    <!-- 3. Print the names added so far in the student array. -->
    <p><?php print_r($students); ?></p>

    <!-- 4. Count the number of names in the student array. -->
    <pre><?php echo count($students) ?></pre>

    <!--  5. Add another student then print the array and its new count. -->
    <!-- Add another student -->
    <?php array_push($students, 'Jane Smith'); ?>

    <!-- print the array and its new count. -->
    <p><?php print_r($students); ?></p>
    <pre><?php echo count($students) ?></pre>

    <!-- 6. Finally, remove the first student and print the array and its count. -->
    <!-- remove the first student -->
    <?php array_shift($students); ?>

    <!-- print the array and its count -->
    <p><?php print_r($students); ?></p>
    <pre><?php echo count($students) ?></pre>


</body>
</html>