<?php

/*  - Loops
        1. Using loops, print all numberss that are divisible bby 5.
        2. Stop the loop when the loop reaches its 100th iteration.
    -
*/

function whileLoop() {

    $count = 0;

    while($count <= 100){

        echo $count. ',';
        $count+=5;
    }
}

/* 
    - Arrays
        1. Create an empty array named "students".
        2. Accept a name of the student and add it to the student array.
        3. Print the names added so far in the student array.
        4. Count the number of names in the student array.
        5. Add another student then print the array and its new count.
        6. Finally, remove the first student and print the array and its count.
    -
*/

/* 1. Create an empty array named "students". */
$students = [];

