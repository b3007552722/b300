
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S02 Discussion: Repetion Control Structures and Array Manipulation.</title>
</head>
<body>
    <h1>Repetion Control Structures</h1>

    <h2>While Loop</h2>
    <?php whileLoop(); ?>
    <!-- This is used to invoke the whileLoop() function and display the output in webpage -->

    <h2>Do-While Loop</h2>
    <?php doWhileLoop(); ?>

    <h2>For Loop</h2>
    <?php forLoop(); ?>

    <h2>Continue and Break</h2>
    <?php modifiedForLoop(); ?>

    <h1>Array Manipulation</h1>
    <h2>types of Arrays</h2>

    <h3>Simple Array</h3>
    <ul>
            <?php foreach($computerBrands as  $brand) {?>
                <li><?php echo $brand ?></li> 
            <?php } ?>
    </ul>

    <h3>Associative Array</h3>
    <ul>
            <?php foreach($gradePeriods as  $period => $grade) {?>
                <li>Grade in <?= $period ?> is <?= $grade ?>.</li> 
            <?php } ?>
    </ul>

    <h3>Multidimensional Array</h3>
    <ul>
        <?php
            foreach($heroes as $team){
                foreach($team as $member){
                    ?>
                    <li><?php echo $member ?></li>
                    <?php }
                }
        ?>
    </ul>

    <h1>Array Functions</h1>
    <h3>Sorting</h3>
    <pre><?php print_r($sortedBrands); ?></pre>

    <h1>Array Functions</h1>
    <h3>Sorting</h3>
    <pre><?php print_r($reverseSortedBrand); ?></pre>

    <h3>Append</h3>
    <?php array_push($computerBrands, 'Apple'); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    <?php array_unshift($computerBrands, 'Alienware'); ?>
    <pre><?php print_r($computerBrands); ?></pre>

    <h3>Remove</h3>
    <?php array_pop($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>
    <?php array_shift($computerBrands); ?>
    <pre><?php print_r($computerBrands); ?></pre>

    <h3>Others</h3>
    <h4>Count</h4>
    <pre><?php echo count($computerBrands) ?></pre>

    <h4>In Array</h4>
    <p><?php echo seachBrand($computerBrands, 'HP'); ?></p>
    <p><?php echo seachBrand($computerBrands, 'Cherry'); ?></p>

    <h4>Reverse (not Z-A sorting)</h4>
    <pre><?php print_r($reverseGradePeriods); ?></pre>

</body>
</html>