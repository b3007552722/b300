<?php

/*  - [1] Repetition Control Structures
        Repetition Control structures are used to ecute code multiple times.
    -
    - While Loop
        takes a single condition. 
        If the condition evaluates to true, the code inside the block will run.
    -
    - Do-while Loop
        it works a lot like the while loops but unlike while loops, do while loops guarantee that the code will be executed at least once.
    -
    - For Loop
        For Loop ismore flexible than While and Do-While Loop
        consists of 3 part:
        1. intitial value
        2. condition
        3. iteration
    -
    - Preferred usage of While vs for Loops
        For loops - are best used when the number of iteration is known beforehand (iterating over arrays).

        While Loops - are better when number of iterations is unknown at the time the loop is define (dependes on user input).
    -
    - Continue and Break Statement
        Cotinue is keyword that allwos the code to go to the next loop without finishing the current code block.

        Break is a keywork that ends the execution of the current loop.
    -
*/

/* While Loop */

function whileLoop() {

    $count = 5;

    while($count !== 0){

        echo $count. '<br/>';
        $count--;
    }
}

/* Do-while Loop */

function doWhileLoop(){

    $count = 20;

    do {
        echo $count. '<br/>';
        $count--;
    }while($count > 0);

};

/* For Loop */

function forloop() {

    for($count = 0; $count <=20; $count++){
        echo $count. '<br/>';
    }
};

/* Continue and Break Statement */

function modifiedForLoop(){

    for($count = 0; $count <=20; $count++){
        if($count % 2 === 0){
            continue;
        }
        echo $count. '<br/>';
        if($count > 10){
            break;
        }
    }
};

/* 
    - [2] Array Manipulation
        An Array is kind of variable that can hold more than one value.

        Arrays are declared using the square brackets '[]' or array() function
    -
*/

$studentNumber = array('2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'); //Before

$studentNumber = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']; //After Introduced on PHP 5.4

// Simple Arrays
$grades = [98.5, 94.3, 89.2, 90.1];
$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu', 'Dell', 'Hp'];
$task = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake react'
];

// Associative Array
$gradePeriods =  ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Two-Dimensional Array

$heroes = [

    ['ironman', 'thor', 'hulk'],
    ['wolverine', 'cyclops', 'jean grey'],
    ['batman', 'superman', 'wonderwoman']

];

// 2-dimensional associative array

$ironManPowers = [
    'regular' => ['repulsor blast', 'rocket punch'],
    'signature' => ['unibeam']
];


// Array Mutations

$sortedBrands = $computerBrands;
$reverseSortedBrand = $computerBrands;

sort($sortedBrands);

rsort($reverseSortedBrand);

function seachBrand($brands, $brand){
    return(in_array($brand, $brands)) ? 
    "$brand is in the array." 
    : 
    "$brand is not in the array.";
}

$reverseGradePeriods = array_reverse($gradePeriods);